# [*Brain Imaging and Functional MRI*](http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/imagerie-fonctionnelle-cerebrale-et-interface-cerveau-machine-161979.kjsp) TP (Master MVA)
## *TP Brain Imaging* : **Convergence of the SpaceNet classifier**

### Meta-data:
- *Subject*: **Decoding with SpaceNet: face vs house object recognition** (and other binary problems);
- *Advisor*: [Bertrand Thirion](https://team.inria.fr/parietal/bertrand-thirions-page/);
- *Keywords*: Python, nilearn, fMRI, SpaceNet, TV-L1, Graph-Net;
- *Where*: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva16-tp-brain-imaging-nilearn).

### Feedback:
- **Grade**: I got 17.64/20 for this course, and 18/20 for this project;
- **Rank**: 2nd amongst 14 students who got a grade (for them, average was 11.01/20).

-----

## Short overview of the goal of the TP
Here is an example of decoding brain imaging with a SpaceNet prior (i.e Graph-Net, TV-L1, etc.), reproducing the Haxby 2001 study on a face vs house discrimination task.

- This script was originally on: [plot_haxby_space_net.html](https://nilearn.github.io/auto_examples/02_decoding/plot_haxby_space_net.html).
- See also [the SpaceNet documentation](https://nilearn.github.io/decoding/space_net.html#haxby).
- [Also on github](https://github.com/nilearn/nilearn/blob/master/examples/02_decoding/plot_haxby_space_net.py).

The goal was to study the SpaceNet classifier, and mainly the influence of the ``tol`` parameter on the speed of convergence and accuracy of the prediction (``tol`` is the tolerance parameter in the [classifier algorithm](https://nilearn.github.io/decoding/space_net.html#haxby)).

**Spoiler : both CPU time and prediction accuracy increase when the tolerance goes to 0.**

-----

### Example of predictions
> Here are displayed tow views of the brain, highlighting where are the important voxels involved in the differentiation between *"house"* and *"face"*, or between *"scissors"* and *"chair"*:

#### Face vs House
An "easy" detection problem:

##### Face vs House, with Graph-Net penalty, tol=1e-4 : accuracy = 98.1%
![Face vs House, with Graph-Net penalty, tol=1e-4 : accuracy = 98.1%](./fig/Haxby__face_vs_house__Graph-Net__accuracy_98_1481.png "Face vs House, with Graph-Net penalty, tol=1e-4 : accuracy = 98.1%")

##### Face vs House, with TV-L1 penalty, tol=1e-4 : accuracy = 96.3%
![Face vs House, with TV-L1 penalty, tol=1e-4 : accuracy = 96.3%](./fig/Haxby__face_vs_house__TV-L1__accuracy_96_2963.png "Face vs House, with Graph-Net penalty, tol=1e-4 : accuracy = 96.3%")


#### Scissors vs Chair
A "hard" detection problem:

##### Scissors vs Chair, with Graph-Net penalty, tol=1e-4 : accuracy = 59.3%
![Scissors vs Chair, with Graph-Net penalty, tol=1e-4 : accuracy = 59.3%](./fig/Haxby__scissors_vs_chair__Graph-Net__accuracy_59_2593.png "Scissors vs Chair, with Graph-Net penalty, tol=1e-4 : accuracy = 59.3%")

##### Scissors vs Chair, with TV-L1 penalty, tol=1e-4 : accuracy = 60.2%
![Scissors vs Chair, with TV-L1 penalty, tol=1e-4 : accuracy = 60.2%](./fig/Haxby__scissors_vs_chair__TV-L1__accuracy_60_1852.png "Scissors vs House, with Graph-Net penalty, tol=1e-4 : accuracy = 60.2%")

-----

### Results
> Here are displayed two graphs of the final experiment:

#### Increase of the computation time as the tolerance decreases
![Increase of the computation time as the tolerance decreases](./fig/Plot_Computation_time_with_Graph-Net__N=9__face_vs_house.png "Increase of the computation time as the tolerance decreases")

#### Increase of the prediction accuracy time as the tolerance decreases
![Increase of the prediction accuracy time as the tolerance decreases](./fig/Plot_Accuracy_with_Graph-Net__N=9__face_vs_house.png "Increase of the prediction accuracy time as the tolerance decreases")

> These graphs were obtain in about 28 minutes on a 8-cores and 16 GB Ram Debian (jessie) machine.

-----

## Files in this repo
### [Code](./TD11__Convergence_of_the_Spacenet_classifier.py)
See [the script](./TD11__Convergence_of_the_Spacenet_classifier.py), fully commented and very detailed,
[its requirements](./requirements.txt), and [example of output](./TP_Brain_Imaging__Lilian_Besson__03-2016.output.txt).

### [Requirements](./requirements.txt)
The code is written in [Python](https://www.python.org/), working on both [v2](https://docs.python.org/2.7/) and [v3](https://docs.python.org/3.5/).
The following scientific Python modules are required:

- *scipy* >= 0.16
- *numpy* >= 1.9
- *matplotlib* >= 1.5
- *scikit-learn* >= 0.17
- *nilearn* >= 0.2

> - You can install them with ``pip install -r requirements.txt``.
> - The code of [the main script](./TD11__Convergence_of_the_Spacenet_classifier.py) is [pep8/pylint valid](https://www.pylint.org/), with [a score of **9.71/10**](./TD11__Convergence_of_the_Spacenet_classifier.pylint.txt).

### How to reproduce the results ?
> [Use this Makefile](./Makefile)

You can clone the repository, install the required Python module, and run the complete experiment with these commands:

```bash
$ git clone https://bitbucket.org/lbesson/mva16-tp-brain-imaging-nilearn
$ cd mva16-tp-brain-imaging-nilearn/
$ make install
$ make nicetest
```

### Example of output for this experiment
> [See this log text file](./TP_Brain_Imaging__Lilian_Besson__03-2016.output.txt) for an example.

### [TP report in LaTeX](./TD11__Convergence_of_the_Spacenet_classifier.tex)
> - About 5 pages, in French (sorry). It was due April 1st (2016),

See [here for the (final) PDF report](https://bitbucket.org/lbesson/mva16-tp-brain-imaging-nilearn/downloads/TP_Brain_Imaging__Lilian_Besson__03-2016.en.pdf).

-----

## About this project
This project was done for the [*Brain Imaging and Functional MRI*](http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/imagerie-fonctionnelle-cerebrale-et-interface-cerveau-machine-161979.kjsp) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

> [A few git statistics](./complete-stats.txt) | [Git Makefile](./Makefile)

### Copyright
©, 2016, [Lilian Besson](http://perso.crans.org/besson/) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project has been publicly published the 20-03-16, under the terms of the [MIT license](http://lbesson.mit-license.org/).
