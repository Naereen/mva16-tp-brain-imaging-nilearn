%% -*- mode: latex; coding: utf-8 -*-
%% Start of LaTeX source for TD11__Convergence_of_the_Spacenet_classifier.tex, in English.
%autotex% Title: TP Brain Imaging : Convergence of the Spacenet classifier -- Lilian Besson
%autotex% Scale: 0.80
%autotex% PoliceSize: 11pt

\section*{\textbf{TP Brain Imaging} : Convergence of the Spacenet classifier -- Lilian Besson}

    \begin{quote}
        Veuillez trouver en pièce jointe le code Python pour le TD\#11\footnote{ \url{https://nilearn.github.io/auto_examples/02_decoding/plot_haxby_space_net.html}.} du cours \href{http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/imagerie-fonctionnelle-cerebrale-et-interface-cerveau-machine-161979.kjsp}{``Functional Brain Imaging''} du \href{http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/}{Master MVA}.
        Voici le rapport concernant ce sujet de TD\#11.
    \end{quote}


    Le code, rapport et les illustrations sont aussi distribuées sous licence libre (\href{http://lbesson.mit-license.org/}{MIT}),
    sur \href{https://bitbucket.org/lbesson/mva16-tp-brain-imaging-nilearn}{ce dépôt git sur Bitbucket} :
    \begin{center}
        \url{https://bitbucket.org/lbesson/mva16-tp-brain-imaging-nilearn}
    \end{center}


    \paragraph{Vulgarisation de l'expérience :}
    \begin{small}
        Nous utilisons des données d'une expérience de neuro-imagerie datant de $2001$ :
        $6$ patients auxquels on a montré des petits dessins, de différentes catégories (un chat, une maison, un visage etc), [Haxby 2001].
        Pendant qu'ils regardent ces dessins, l'activité de leur cerveau est enregistrée par un IRM fonctionnel, durant plusieurs sessions, et stockée sous forme d'``images''.

        Ensuite le but est de ``lire dans leurs pensées'' : prédire quelle catégorie ils regardaient (chat, maison, chaise, ciseau etc), selon l'apparence générale de la carte d'activité de leur cerveau.
        Ces données ont la forme de $216$ ``photos en 3D'' de leur cerveau ($40 \times 64 \times 64$ voxels), qu'on sépare $144$ photos et étiquettes, pour apprendre le modèle mathématiques, et 72 photos (sans étiquette) pour tester la prédiction, ce qui donne un score en $\%$ de réussite.

        Le but de cette expérience numérique d'apprentissage supervisé est de confirmer que le cerveau moyen (des 6 patients) réagit différemment à des catégories différentes,
        et de démontrer l'efficacité de l'outil mathématique d'apprentissage (un classifieur ``SpaceNet'').
	%
        Par exemple, différencier ``chat'' vs. ``maison'' ou ``visage'' vs. ``maison'' fonctionne très bien (98\% de réussite).
        Mais il sera plus difficile de différencier ``bouteille'' vs. ``ciseau'', ou ``chaise'' vs. ``ciseau'' (seulement 67\% de réussite).
    \end{small}

% ----------------------------------------------------------------------------------------------------------------
\hr{}

\subsection*{Remarques préliminaires}
\begin{itemize}
    \item Le TD se focalisait sur la convergence du classificateur SpaceNet, dont la documentation se trouve
        \href{https://nilearn.github.io/modules/generated/nilearn.decoding.SpaceNetClassifier.html#nilearn.decoding.SpaceNetClassifier}{ici si nécessaire}
        et \href{https://nilearn.github.io/decoding/space_net.html#space-net}{un tutoriel se trouve là}
        (sur \href{https://nilearn.github.io/}{nilearn.github.io}).

    \item Le sujet de TD suggérait d'exécuter le code sur un serveur distant, ce que j'ai fait sur le serveur
        \href{https://wiki.crans.org/CransTechnique/LesServeurs/ServeurZamok}{zamok}\footnote{ Debian jessie, 16 Gb RAM, 8 cœurs à 3.2 GHz.}, merci à l'\href{https://www.crans.org/}{association CRANS} de l'\href{http://www.ens-cachan.fr/}{ENS Cachan}.
        J'ai utilisé Python\footnote{ Pour la reproductibilité : \href{https://docs.python.org/3.5/}{Python v3.5.1}, \href{http://www.ipython.org/}{IPython v4.1.1}, \href{https://nilearn.github.io/}{nilearn v0.2.3}, \href{https://www.numpy.org/}{numpy v1.10.4}, \href{https://www.scipy.org/}{scipy v0.17,} \href{https://www.matplotlib.org/}{matplotlib v1.5.1}, \href{http://scikit-learn.org/}{scikit-learn v0.17.1}. Notez que mon code est normalement compatible avec \href{https://docs.python.org/2.7/}{Python v2.7+}.}
        3, installé avec \href{http://conda.pydata.org/miniconda.html}{miniconda} v4.0.5.

    \item Le sujet utilisait les \href{http://data.pymvpa.org/datasets/haxby2001/}{données de la base Haxby2001} :
    \url{http://data.pymvpa.org/datasets/haxby2001/}, issue de \href{http://www-psych.stanford.edu/~jbt/205/Haxby_etal01.pdf}{cet article [Haxby et. al., 2001]}.
        Ces données sont obtenues en Python avec la fonction \verb+nilearn.datasets.fetch_haxby+ (environ 315 Mo).
\end{itemize}


% ----------------------------------------------------------------------------------------------------------------
\hr{}

    Les exemples\footnote{ Les autres graphiques sont tous \href{https://bitbucket.org/lbesson/mva16-tp-brain-imaging-nilearn/src/master/fig/}{dans le dossier \texttt{fig}} fournit dans l'archive \texttt{zip} envoyé avec ce TP, notamment pour la question 2.}
    ci-dessous montrent les résultats obtenus pour les questions 1, 2 et 3 du TP \#11.


\subsection*{Question 1}

    Quelques détails concernant les paramètres des simulations:
    \begin{itemize}
        \item
        Comme suggéré, j'ai demandé au \href{https://nilearn.github.io/modules/generated/nilearn.decoding.SpaceNetClassifier.html#nilearn.decoding.SpaceNetClassifier}{SpaceNetClassifier} d'utiliser le nombre maximal de
        cœurs\footnote{ Et pour ne pas sur-exploiter toutes les ressources du serveur multi-utilisateurs que j'ai utilisé, j'ai exécuté le script avec \texttt{nice -n 19}, pour \href{https://en.wikipedia.org/wiki/Nice_(Unix)}{sélectionner la priorité la plus faible}, cf. le script \texttt{Makefile} joint avec le code.},
        en choisissant\footnote{ Notez que ce morceau de code doit être bogué, comme le processus utilisait quand même $100\%$ des 8 cœurs si je demandais \texttt{n\_jobs = 1} (un seul job, un seul cœur normalement).} \texttt{n\_jobs = -1}.

        \item
        \verb+nb_chunks_train = 7+, utilisé pour séparer les données en deux morceaux (entraînement, test), selon la condition \verb+labels['chunks'] <= nb_chunks_train+ ou \verb+> nb_chunks_train+.
            Plus cette valeur est grande, plus on a de données étiquetées d'entraînement,
            et donc plus la précision sur les données de test sera grande (observation classique, c'est une tâche d'apprentissage supervisé).
            Pour information, avec cette valeur de \verb+nb_chunks_train+,
            les quatre vecteurs de données \verb+X_train, X_test, y_train, y_test+ ont les dimensions suivantes :

        \begin{center}
            \begin{tabular}{|l|r|}
                \hline
                Vecteur & dimensions \\
                \hline
                \verb+X_train+  &  $(40, 64, 64, 144)$ \\
                \verb+X_test+   &  $(40, 64, 64, 72)$ \\
                \verb+y_train+  &  $(144,)$ \\
                \verb+y_test+   &  $(72,)$ \\
                \hline
            \end{tabular}
        \end{center}

            Nous sommes effectivement dans un cas typique de ``haute dimension'' :
            144 (resp. 72) données en dimension d'entraînement (resp. de test)
            de dimensions égales à
            $x \times y \times z = 40 \times 64 \times 64 = 163840$.

        \item
        J'ai testé à la fois la pénalisation ``Graph-Net'' et ``TV-L1''. Comme annoncé dans le sujet de TP, la première est plus rapide que la seconde ($3 \times +$), elles donnent en général la même précision, mais la seconde donne des ``features maps'' plus satisfaisante graphiquement (plus régulière, avec de grandes zones constantes, c'est bien une image type ``cartoon''), cf. ci dessous.

        \item
        La première tâche correspondait à implémenter une expérience de classification binaire,
        implémentée par la fonction \verb+oneExperiment+, qui accepte les paramètres suivants :
        \begin{small}
            \begin{lstlisting}[language=python, fontadjust=false]
oneExperiment(X_train, X_test, y_train, y_test, background_img,  # Data
              target1=b'face', target2=b'house',   # Binary pb
              SHOW=True, tol=1e-4, verbose=1, nb_chunks_train=7,
              use_graph_net=True, use_tv_l1=False) # Options
            \end{lstlisting}
        \end{small}

        \item
        Les ``features maps'' ont été affichées avec la fonction \verb+nilearn.plotting.plot_stat_map+,
            et les paramètres \verb+cut_coords=(-34, -16), display_mode="yz"+.
        Ci-dessous à la question 2 se trouvent quelques ``features maps'',
        pour différents problèmes de classification, et différentes pénalisations.

    \end{itemize}


    J'ai commencé par considérer le problème de classification binaire ``facile'', ``house'' vs. ``face'',
    puis un autre ``facile'', ``house'' vs. ``cat'';
    et après j'ai regardé le problème annoncé comme plus difficile, ``scissors'' vs. ``chair''.
    On constate effectivement que la convergence est plus lente à atteindre, et que la précision obtenue est bien moins bonne (entre $95\%$ et $98\%$ pour les problèmes faciles, mais seulement $65\%$ pour le plus différentes).


    Cf. \href{http://www-psych.stanford.edu/~jbt/205/Haxby_etal01.pdf}{l'article initial [Haxby et. al., 2001]} pour plus de détails concernant les données Haxby2001.
    Voici quelques exemples de stimuli visuels montrés aux sujets de cette expérience, classés par catégorie (``house'', ``scissors'', ``cat'', ``chairs'' etc) :
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.35\textwidth]{fig/Haxby2001__face_house_cat_chair.png}
        \caption{(Qu1) Stimuli visuels utilisés dans l'article [Haxby et. al., 2001].}
    \end{figure}

    Cette première expérience est implémentée par la fonction \verb+experiment1()+ de mon script,
    qui accepte comme argument \verb+targets_to_try+, une liste de problèmes binaires à considérer.


% ----------------------------------------------------------------------------------------------------------------
\hr{}
\subsection*{Question 2}

    Dans ce question, on essaie plusieurs valeurs du paramètre \texttt{tol},
    selon une grille décroissante, géométrique (mais non régulièrement espacée),
    % avec $4$ valeurs entre \texttt{1} et \texttt{1e-5} :
    avec $9$ valeurs entre \texttt{1} et \texttt{1e-5} :
    % avec $17$ valeurs entre \texttt{1} et \texttt{1e-7} :
    \begin{small}
        \begin{lstlisting}[language=python, fontadjust=false]
grid_tol = [1, 0.5, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0001, 1e-05]
        \end{lstlisting}
    \end{small}
% grid_tol = [10**i for i in range(0, -6, -1)]
% grid_tol = [1, 0.01, 0.001, 1e-5]
    % \begin{center}
        % \verb/grid_tol = [1, 0.5, 0.1, 0.05, 0.01, 0.005, 0.001, 5e-4, 1e-4, 5e-5, 1e-5, 5e-6, 1e-6, 5e-7, 1e-7]/.\\
        % En Python :
        % \verb/grid_tol = sorted([10**i for i in range(0, -8, -1)] + [5*10**i  for i in range(0, -8, -1)], reverse=True)/\\
        % Ou bien pour une grille logarithmique régulièrement espacée :
        % \verb+grid_tol = np.logspace(1, -8, 17)+
            % grid_tol = np.logspace(1, -6, 13)
    % \end{center}


    J'ai uniquement essayé le problème de classification binaire le plus simple,
    ``face'' vs. ``house'', et uniquement la pénalisation ``Graph-Net'' (la plus rapide),
    pour garder un temps de calcul raisonnable (de l'ordre de $28$ minutes).

    Pour moins de valeurs de \texttt{tol} (\verb+grid_tol = [1e-1, 1e-3, 1e-7]+),
    j'avais tenté les deux pénalisation ``Graph-Net'' et ``TV-L1'', mais cette dernière est trop lente
    (environ 85 minutes pour un problème binaire, trois valeurs de \texttt{tol} et les deux pénalisations).
    En fait, ``TV-L1'' devient très lente pour des tolérance trop petite.
    J'ai aussi constaté que sur ces données, avec ces paramètres, diminuer la tolérance au dessous de \texttt{1e-5} ne change pas \emph{du tout} le score final,
    mais augmente (beaucoup) le temps de calcul (donc je m'arrête à \texttt{1e-5}).

    Cette deuxième expérience est implémentée par la fonction \verb+experiment2()+ de mon script,
    qui accepte comme paramètres \verb+grid_tol, target1, target2+,
    une grille de valeur de \texttt{tol} à consider, et un problème de classification binaire.

    Notez que pour les deux premières courbes ci-dessous,
    l'axe $x$ correspond à la tolérance, en échelle \emph{anti-logarithmique} (\texttt{x = - np.log10(tol)}),
    \texttt{tol} \emph{diminue} vers la droite.


\subsubsection*{Temps de calcul en fonction de la tolérance}

    De façon assez logique, le temps de calcul augmente strictement quand on diminue la tolérance :
    une faible \texttt{tol} demande une convergence plus précise et donc plus de calcul.
    De façon assez surprenante, le temps de calcul est \emph{quasi-linéaire} en fonction de \texttt{-log(tol)},
    pour les premières valeurs.

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.58\textwidth]{fig/Plot_Computation_time_with_Graph-Net__N=9__face_vs_house.png}
        \caption{(Qu2) Temps de calcul en secondes, en fonction du paramètre \texttt{tol} (9 valeurs essayées).}
    \end{figure}


\subsubsection*{Précision en fonction de la tolérance}

    Et de même, de façon très cohérente, la précision augmente (strictement) quand on diminue le paramètre \texttt{tol}.
    Notez que la précision est bornée, par $100\%$ au maximum, et qu'il est irréaliste d'espérer atteindre $100\%$.
    Une précision de $96\%$--$98\%$ telle qu'obtenue pour \texttt{tol = 1e-4} est déjà excellente !

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.58\textwidth]{fig/Plot_Accuracy_with_Graph-Net__N=9__face_vs_house.png}
        \caption{(Qu2) Précision en $\%$, en fonction du paramètre \texttt{tol} (9 valeurs essayées).}
    \end{figure}



\subsubsection*{Quelques ``features maps''}

    Voici quelques ``features maps'' pour différentes valeurs de ce paramètre \texttt{tol},
    comparant la pénalisation ``Graph-Net'' et ``TV-L1'' :

    \paragraph{``face'' vs. ``house'' :}
        Pour la même tolérance, ``Graph-Net'' donne un meilleur score que ``TV-L1'',
        mis une feature map moins régulière.
    \begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=0.98\textwidth]{fig/Haxby__face_vs_house__Graph-Net__accuracy_98_1481.png}
        \caption{(Qu2) Pénalisation ``Graph-Net'' et \texttt{tol = 1e-4} : précision $98.1\%$.}
        \end{subfigure}
        ~  % Space
        \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \includegraphics[width=0.98\textwidth]{fig/Haxby__face_vs_house__TV-L1__accuracy_96_2963.png}
        \caption{(Qu2) Pénalisation ``TV-L1'' et \texttt{tol = 1e-4} : précision $96.3\%$.}
        \end{subfigure}
    \end{figure}


    \paragraph{``cat'' vs. ``house'' :}
        Pour la même tolérance, ``Graph-Net'' donne cette fois un moins bon score que ``TV-L1''.
    \begin{figure}[H]
        \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \includegraphics[width=0.98\textwidth]{fig/Haxby__cat_vs_house__Graph-Net__accuracy_94_4444.png}
        \caption{(Qu2) Pénalisation ``Graph-Net'' et \texttt{tol = 1e-4} : précision $94.4\%$.}
        \end{subfigure}
        ~  % Space
        \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \includegraphics[width=0.98\textwidth]{fig/Haxby__cat_vs_house__TV-L1__accuracy_98_1481.png}
        \caption{(Qu2) Pénalisation ``TV-L1'' et \texttt{tol = 1e-4} : précision $98.1\%$.}
        \end{subfigure}
    \end{figure}


    \paragraph{``scissors'' vs. ``chair'' :}
        Problème plus difficile, la précision est bien moins satisfaisante :
    \begin{figure}[H]
        \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \includegraphics[width=0.98\textwidth]{fig/Haxby__scissors_vs_chair__Graph-Net__accuracy_63_8889__tol_0_0001.png}
        \caption{(Qu2) Pénalisation ``Graph-Net'' et \texttt{tol = 1e-5} : précision $63.9\%$.}
        \end{subfigure}
        ~  % Space
        \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \includegraphics[width=0.98\textwidth]{fig/Haxby__scissors_vs_chair__TV-L1__accuracy_65_2778__tol_0_0001.png}
        \caption{(Qu2) Pénalisation ``TV-L1'' et \texttt{tol = 1e-5} : précision $65.3\%$.}
        \end{subfigure}
    \end{figure}


% ----------------------------------------------------------------------------------------------------------------
\hr{}
\subsection*{Question 3}

    Comme souvent en apprentissage statistique et en mathématiques appliquées,
    adapter ce paramètre \texttt{tol} demande un compromis (``trade-off'')
    entre temps de calcul et qualité du résultat (comme observé dans les graphiques de la question 2) :
    \begin{itemize}
        \item Une trop grande valeur \texttt{tol} donnera une convergence rapide mais pas très précise;
        \item Et inversement, une trop petite valeur \texttt{tol} donnera une convergence très lente mais bien plus précise.
    \end{itemize}


    Une valeur par défaut pour \texttt{tol} ne peut pas être choisie pour n'importe quel problème.
    Dans certains cas, on aimerait une convergence rapide et des résultats approximatifs,
    dans d'autres on souhaite des résultats fiables (et on peut se permettre de laisser calculer longtemps).


    On peut néanmoins imaginer qu'une valeur ``intermédiaire'' entre \texttt{1} et \texttt{1e-8}, par exemple \texttt{1e-4},
    offre en général un bon compromis entre ces deux aspects de temps de calcul et de précision de la classification.
    Notez que c'est cette valeur qui est mise par défaut dans tous les classificateurs implémentés par \texttt{nilearn} et \texttt{scikit-learn},
    cf. par exemple \href{http://scikit-learn.org/dev/modules/generated/sklearn.linear_model.Ridge.html#sklearn.linear_model.Ridge}{\texttt{sklearn.linear\_model.Ridge}}.
    % ainsi que dans \href{http://cvxopt.org/userguide/coneprog.html#algorithm-parameters}{cvxopt}.


    Une bonne pratique pour choisir un paramètre de tolérance\footnote{ Aussi appelé $\varepsilon$ dans la plupart des algorithmes en mathématiques appliqués.}
    comme \texttt{tol} consiste par commencer par une valeur relativement grande, comme \texttt{1e-2},
    pour avoir rapidement des premiers résultats approximatifs assez rapidement;
    puis diminuer progressivement \texttt{tol} jusqu'à avoir de meilleurs résultats,
    et s'arrêter quand la convergence est trop lente à obtenir.

    On peut aussi envisager une \href{http://scikit-learn.org/dev/modules/grid_search.html#grid-search}{exploration automatique d'une grille de différentes valeurs} de \texttt{tol},
    par \href{http://scikit-learn.org/dev/modules/cross_validation.html#cross-validation}{validation croisée}
    (ce qui peut être fait avec scikit-learn via \href{http://scikit-learn.org/dev/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV}{\texttt{GridSearchCV}} notamment).


% ----------------------------------------------------------------------------------------------------------------
\hr{}
\subsection*{Conclusion}

    Ce TP a été l'occasion d'étudier la convergence du classificateur SpaceNet,
    et notamment l'influence du paramètre \texttt{tol} sur la vitesse de convergence et la précision du classificateur obtenu.
    Les résultats observés dans cette expérience avec la base de donnée Haxby, pour un problème de classification binaire (``house'' vs. ``scissors'', or ``scissors'' vs. ``chair''),
    ont confirmé l'intuition initiale : le temps de calcul et la précision augmentent quand \texttt{tol} diminue vers $0$.


% ----------------------------------------------------------------------------------------------------------------
\hr{}
\vfill{}
\begin{quote}
    \hfill{}
    \emph{Je reste bien sûr à votre disposition, si besoin.}
\end{quote}

%% End of LaTeX source.
